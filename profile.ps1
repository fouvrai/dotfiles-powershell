
#
# Variables
#

$editor = "gvim"

#
# Functions
#

function Edit-File($path) {
    & $editor $path
}

function Remove-Alias($alias) {
    Remove-Item alias:$alias -Force -ErrorAction SilentlyContinue
}

. Remove-Alias where
function where($exe) {
    Get-Command $exe | % path
}

#
# UI
#

function Prompt {
    $location = $executionContext.SessionState.Path.CurrentLocation
    return "`n[$location]`n> "
}

#
# Profile
#

# the profile i care about most is CurrentUserAllHosts
$profile | Add-Member ScriptMethod ToString { $this.CurrentUserAllHosts } -Force

function Reload-Profile {
    @(
        $profile.AllUsersAllHosts,
        $profile.AllUsersCurrentHost,
        $profile.CurrentUserAllHosts,
        $profile.CurrentUserCurrentHost
    ) | % {
        if (Test-Path $_) {
            . $_
        }
    }
}

function Edit-Profile {
    Edit-File $profile
}

#
# Aliases
#

Set-Alias reload  Reload-Profile
Set-Alias profile Edit-Profile

